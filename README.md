# Experiments with Metropolis within Gibbs

`mcmc_monoid` presents a monoidal pattern to compose a series of 
atomic MCMC kernels together.  The idea is to have a common interface
for a node or leaf of a binary tree, traversal of which represents the
kernel composition.  The code makes use of Tensorflow Probability, though
it would be nice to abstract completely the library implementing
atomic kernels. 

## Python environment

This project is managed by [Poetry](https://python-poetry.org), which 
needs to be installed in order to build the virtual environment.

From the repo top level directory

```bash
$ poetry install
```
to install the dependent packages.

## Test

An example of using the `MCMCMonoid` pattern is given in 
`mcmc_monoid/compound_kernel_test.py`.  To run this test
use `pytest`, e.g.

```python
$ poetry run pytest
```
