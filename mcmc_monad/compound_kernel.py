"""Monad-based Metropolis-within-Gibbs framework

The idea is to wrap atomic `tfp.mcmc.TransitionKernel` objects
with MCMCStep, and then provide a monoidal interface to compose
multiple MCMCSteps into a tree-like structure for algorithm 
execution.  The monoid pattern recognises that the composition
of a sequence of MCMC transition kernels is itself a 
transition kernel, such that the interface to a composite
kernel should be exactly the same as for that of its
component samplers.

Current limitations:

    1. Each MCMCStep must be instantiated with the joint log
       probability function, rather than receiving it from the
       tree walking algorithm in realtime.  To enable this would
       mean passing the function to `bootstrap_results` and
       `one_step` methods.
    2. The framework does _not_ currently cache the value of the
       target densities from one kernel invocation to the next.  This
       could be done by `MCMCTreeNode.one_step` with the disadvantage
       of tighter coupling to the underlying atomic transition kernel
       library.
"""

from __future__ import annotations
from typing import Callable, List, Tuple
from collections import namedtuple

import tensorflow_probability as tfp


def gather_state_parts(state_parts):
    """Create a function which slices a state namedtuple"""

    def fn(current_state):
        return (current_state[x] for x in state_parts)

    return fn


def is_list_like(x):
    """Return True if `x` is a tuple or list"""
    return isinstance(x, (tuple, list))


MCMCStepResults = namedtuple("MCMCStepResults", ["name", "inner_results"])


class MCMCMonoid:
    """The Abstract MCMCMonoid class

    The MCMC monoid class is implemented in Python
    as a binary tree.  This class provides an abstract
    interface for both MCMCSteps (leaves) and MCMCTreeNodes (nodes)
    as well as a combination (__mul__) operator.
    """

    def one_step(
        self,
        current_state: namedtuple,
        previous_results: namedtuple,
        seed: List[int, int] = None,
    ):
        raise NotImplementedError()

    def bootstrap_results(self, current_state: namedtuple) -> namedtuple:
        raise NotImplementedError()

    def __mul__(self, next_kernel: MCMCMonoid) -> MCMCMonoid:
        """Kernel composition operator

        This operator returns a new MCMCTreeNode composing
            `this` and `next_kernel`.
        """
        return MCMCTreeNode(name=self.name, kernel0=self, kernel1=next_kernel)


class MCMCStep(MCMCMonoid):
    def __init__(
        self,
        state_parts: namedtuple,
        joint_target_log_prob_fn: Callable,
        kernel_build_fn: Callable,
        name: str = None,
    ):
        """Implements an MCMCStep

        An `MCMCStep` is a single component of a Metropolis-within-Gibbs
        sampler.

        Args:
            state_parts: a tuple of state part names
            joint_target_log_prob_fn: a function with arguments `state_parts._fields`
                returning the value of the joint log target probability function.
            kernel_build_fn: a function with signature `fn(conditional_log_prob_fn, state)`
                which returns a `tfp.mcmc.TransitionKernel` object to operate on
                `state_parts`.
        """
        state_parts = (
            state_parts if is_list_like(state_parts) else [state_parts]
        )
        if name is None:
            name = "_".join(state_parts)

        self._parameters = dict(
            state_parts=state_parts,
            joint_target_log_prob_fn=joint_target_log_prob_fn,
            kernel_build_fn=kernel_build_fn,
            name=name,
        )

    @property
    def state_parts(self) -> List[str]:
        return self._parameters["state_parts"]

    @property
    def joint_target_log_prob_fn(self) -> Callable:
        return self._parameters["target_log_prob_fn"]

    @property
    def kernel_build_fn(self) -> Callable:
        return self._parameters["kernel_build_fn"]

    @property
    def name(self) -> str:
        return self._parameters["name"]

    def _wrap_results(self, results: namedtuple) -> namedtuple:
        """Wrap results in a MCMCStepResults structure"""
        return MCMCStepResults(self.name, results)

    def _partial_state(self, current_state: namedtuple) -> namedtuple:
        """Returns a PartialState `\subset` current_state"""
        PartialState = namedtuple("PartialState", self.state_parts)

        return PartialState(
            **{k: getattr(current_state, k) for k in self.state_parts}
        )

    def _conditional_log_prob_fn(self, current_state: namedtuple) -> Callable:
        """Returns a partially evaluated target log prob function"""

        def tlp(partial_state):
            full_state = current_state._replace(**partial_state._asdict())
            return self.joint_target_log_prob_fn(**full_state)

        return tlp

    def one_step(
        self,
        current_state: namedtuple,
        previous_results: namedtuple,
        seed: List[int, int] = None,
    ) -> Tuple[namedtuple, namedtuple]:
        """Perform one step of the compound kernel.

        Args:
            current_state: a namedtuple describing a point in the
                joint sample space.
            previous_results: a results namedtuple as returned by
                `bootstrap_results`.

        Returns:
            a tuple of `(next_state, kernel_results)`
        """
        assert isinstance(previous_results, MCMCStepResults)
        assert previous_results.name == self.name

        partial_state = self._partial_state(current_state)

        conditional_kernel = self.kernel_build_fn(
            self._conditional_log_prob_fn(current_state), current_state
        )

        # Call inner_kernel.one_step
        new_partial_state, results = conditional_kernel.one_step(
            partial_state,
            previous_results,
            seed=seed,
        )
        next_state = current_state._replace(**new_partial_state._asdict())

        return next_state, MCMCStepResults(self.name, results)

    def bootstrap_results(self, current_state: namedtuple) -> namedtuple:
        """Return a kernel results structure.

        Args:
            current_state: a namedtuple describing a point in the joint
                sample space.
        """
        partial_state = self._partial_state(current_state)
        kernel = self.kernel_build_fn(
            self._conditional_log_prob_fn(current_state), current_state
        )

        return MCMCStepResults(
            self.name, kernel.bootstrap_results(partial_state)
        )

    def __mul__(self, next_kernel):
        return MCMCTreeNode(name=self.name, kernel0=self, kernel1=next_kernel)

    def __str__(self):
        return f"MCMCStep<{self.name}>({', '.join(self.state_parts)})"


MCMCTreeNodeResults = namedtuple(
    "MCMCTreeNodeResults", ["name", "kernel1", "kernel0"]
)


class MCMCTreeNode(MCMCMonoid):
    def __init__(
        self, kernel0: MCMCMonoid, kernel1: MCMCMonoid, name: str = None
    ):
        """A binary tree node representing the composition of two MCMCSteps

        Args:
            kernel0: the first kernel
            kernel1: the second kernel
        """

        if name is None:
            name = "_".join([kernel0.name, kernel1.name])

        self._parameters = dict(kernel0=kernel0, kernel1=kernel1, name=name)

    @property
    def kernel0(self) -> MCMCMonoid:
        return self._parameters["kernel0"]

    @property
    def kernel1(self) -> MCMCMonoid:
        return self._parameters["kernel1"]

    @property
    def name(self) -> str:
        return self._parameters["name"]

    def one_step(
        self,
        current_state: namedtuple,
        previous_kernel_results: namedtuple,
        seed: List[int, int] = None,
    ) -> Tuple[namedtuple, namedtuple]:

        seed0, seed1 = tfp.random.split_seed(
            tfp.random.sanitize_seed(seed),
            salt="MCMCTreeNode.one_step",
        )

        # TODO: Eliminate the extra bootstrap_results calls if we can
        # rely on the previous_kernel_results to give us the current
        # value of the joint_target_log_prob_fn.
        pkr0 = self.kernel0.bootstrap_results(current_state)
        new_state0, new_results0 = self.kernel0.one_step(
            current_state, pkr0, seed0
        )

        pkr1 = self.kernel1.bootstrap_results(new_state0)
        new_state1, new_results1 = self.kernel1.one_step(
            new_state0, pkr1, seed1
        )

        return new_state1, MCMCTreeNodeResults(
            name=self.name, kernel0=new_results0, kernel1=new_results1
        )

    def bootstrap_results(self, current_state: namedtuple) -> namedtuple:

        return MCMCTreeNodeResults(
            name=self.name,
            kernel0=self.kernel0.bootstrap_results(current_state),
            kernel1=self.kernel1.bootstrap_results(current_state),
        )

    def __str__(self) -> str:
        return f"MCMCTreeNode(name={self.name}, kernel0={str(self.kernel0)}, kernel1={str(self.kernel1)})"
