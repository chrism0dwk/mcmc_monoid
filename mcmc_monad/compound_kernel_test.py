"""Test MCMCTree for Metropolis within Gibbs"""

from collections import namedtuple
import tensorflow_probability as tfp

from mcmc_monad.compound_kernel import MCMCStep


class DummyKernel(tfp.mcmc.TransitionKernel):
    """Mocks up a concrete tfp.mcmc.TransitionKernel"""

    def __init__(self, name):
        self.name = name

    def is_calibrated(self):
        return True

    def one_step(self, current_state, previous_kernel_results, seed=None):
        return current_state, dict(target_log_prob=0.0, is_accepted=True)

    def bootstrap_results(self, current_state):
        return dict(target_log_prob=0.0, is_accepted=False)

    def __repr__(self):
        return f"DummyKernel<{self.name}>"


def test_compound_kernel():
    """Construct MwG sampler for a linear regression

    $$
    y \sim \mbox{Normal}(\alpha + \beta x, \sigma^2)
    $$
    """

    kernel0 = MCMCStep(
        state_parts="alpha",
        joint_target_log_prob_fn=lambda x: 1.0,
        kernel_build_fn=lambda tlp, _: DummyKernel("kernel0"),
    )
    kernel1 = MCMCStep(
        state_parts="beta",
        joint_target_log_prob_fn=lambda x: 1.0,
        kernel_build_fn=lambda tlp, _: DummyKernel("kernel1"),
    )
    kernel2 = MCMCStep(
        state_parts="sigma",
        joint_target_log_prob_fn=lambda x: 1.0,
        kernel_build_fn=lambda tlp, _: DummyKernel("kernel2"),
    )

    # Compose the `compound_kernel` monoid
    compound_kernel = kernel0 * kernel1 * kernel2

    State = namedtuple("State", ["alpha", "beta", "sigma"])

    current_state = State(1.0, 2.0, 3.0)

    results = compound_kernel.bootstrap_results(current_state)
    new_state, new_results = compound_kernel.one_step(current_state, results)
